# js8-cli

Use [JS8Call](http://js8call.com/)'s JSON API via the command line.

This program is essentially just a cli wrapper for [lib-js8call](https://www.npmjs.com/package/@trippnology/lib-js8call).

## Installation

Make sure you have a recent version of [NodeJS](https://nodejs.org/) installed.

`npm install -g @trippnology/js8-cli`

## Usage

```
Usage: js8-cli [options] [command]

Options:
  -v, --vers                             Output the current version
  -h, --host <hostname/IP address>       Hostname or IP address of machine running JS8Call (default: "127.0.0.1")
  -p, --port <port number>               Port used by JS8 host machine (default: "2442")
  --help                                 Display help for command

Commands:
  inbox.get_messages                     View a list of your inbox messages
  inbox.store_message <callsign> <text>  Store a message in your inbox for later transmission
  mode.get_speed                         The current TX speed. 0 = normal, 1 = fast, 2 = turbo, 4 = slow
  rig.get_freq                           The current station frequency
  rig.set_freq <offset> [frequency]      Change the offset and/or frequency
  rx.get_band_activity                   Gets the text in the band activity window
  rx.get_call_activity                   Gets the text in the call activity window
  rx.get_text                            Gets the text in the QSO window
  tx.send_message <text>                 Transmit a message
  tx.set_text <text>                     Set the text in the TX window
  help [command]                         Display help for command

```

More commands coming soon, as they are added to `lib-js8call`.

### Providing dynamic input

You can provide dynamic input to commands in a couple of different ways.

#### Stdin

```bash
echo -n "hello, world!" | js8-cli tx.send_message
# -n option omits the newline at the end
```

```bash
cat some-file.txt | js8-cli tx.send_message
```

#### Command expansion

This method allows more control over the final output.

```bash
js8-cli tx.send_message "$(some-command)"
```

Posting a report to the WX group, using data from the local disk:

```bash
js8-cli tx.send_message "@WX Report from JO02mm: $(cat ~/weather-station/conditions.txt)"
# Outputs:
# @WX Report from JO02mm: 10.5c 93% 1012mb
```

The same method can be used for other types of telemetry (such as battery level), data from web APIs, or whatever else you come up with. Remember to put quotes around anything that may contain spaces.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature develop`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

-   v1.2.1 `--ip` option is now `-h` or `--host`
-   v1.2.0 Add `rx` commands: `rx.get_band_activity`, `rx.get_call_activity`, and `rx.get_text`
-   v1.1.0 Add options for IP (`-i` or `--ip`) and port number (`-p` or `--port`)
-   v1.0.0

## Credits

TODO: Write credits

## License

UNLICENSED
