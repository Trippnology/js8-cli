#!/usr/bin/env node
const _ = require('lodash');
const pkg = require('../package');
const { Command } = require('commander');
const program = new Command();

let js8;
let config = {
	name: 'js8-cli',
};

// Handle piped arguments
const stdin = process.openStdin();
let stdin_data = '';
stdin.on('data', function (chunk) {
	stdin_data += chunk;
});
stdin.on('end', function () {
	// Add to the list of arguments
	process.argv.push(stdin_data);
});

program
	.name(config.name)
	.version(pkg.version, '-v, --vers', 'Output the current version');

program
	.option(
		'-h, --host <hostname/IP address>',
		'Hostname or IP address of machine running JS8Call',
		'127.0.0.1'
	)
	.option(
		'-p, --port <port number>',
		'Port used by JS8 host machine',
		'2442'
	);

program
	.command('inbox.get_messages')
	.description('View a list of your inbox messages')
	.action(() => {
		connect().then(() => {
			js8.inbox.getMessages().then((messages) => {
				_.each(messages, (message) => {
					//console.log(message);
					console.log(
						'%s %s->%s: %s',
						message.params.UTC,
						message.params.FROM,
						message.params.TO,
						message.params.TEXT
					);
				});
				process.exit();
			});
		});
	});

program
	.command('inbox.store_message <callsign> <text>')
	.description('Store a message in your inbox for later transmission')
	.action((callsign, text) => {
		connect().then(() => {
			js8.inbox.storeMessage(callsign, text).then((message) => {
				process.exit();
			});
		});
	})
	.on('--help', function () {
		console.log('');
		console.log('Examples:');
		console.log('');
		console.log(
			'  $ %s inbox.store_message M7GMT "hello, world!"',
			config.name
		);
		console.log(
			'  $ %s inbox.store_message M7GMT "WX Report: $(cat ~/weather/conditions.txt)"',
			config.name
		);
	});

program
	.command('mode.get_speed')
	.description(
		'The current TX speed. 0 = normal, 1 = fast, 2 = turbo, 4 = slow'
	)
	.action(() => {
		connect()
			.then(js8.mode.getSpeed)
			.then((speed) => {
				console.log(speed);
				process.exit();
			});
	});

/*
Currently broken in the API. See: https://bitbucket.org/widefido/js8call/issues/350/mode-button-text-does-not-update
program
	.command('mode.set_speed <speed>')
	.description(
		'Change the TX speed. 0 = normal, 1 = fast, 2 = turbo, 4 = slow'
	)
	.action((new_speed) => {
		connect().then(() => {
			js8.mode.setSpeed(new_speed).then((current_speed) => {
				console.log(current_speed);
				process.exit();
			});
		});
	});*/

program
	.command('rig.get_freq')
	.description('The current station frequency')
	.action(() => {
		connect()
			.then(js8.rig.getFreq)
			.then((freq_data) => {
				delete freq_data._ID;
				let output = [];
				_.each(freq_data, (value, key) => {
					output.push(key + ': ' + value);
				});
				console.log(output.join(', '));
				process.exit();
			});
	});

program
	.command('rig.set_freq <offset> [frequency]')
	.description('Change the offset and/or frequency')
	.action((offset, frequency) => {
		let options = { OFFSET: offset };
		if (frequency) {
			options.DIAL = frequency;
		}
		connect().then(() => {
			js8.rig.setFreq(options).then((freq_data) => {
				delete freq_data._ID;
				let output = [];
				_.each(freq_data, (value, key) => {
					output.push(key + ': ' + value);
				});
				console.log(output.join(', '));
				process.exit();
			});
		});
	});

program
	.command('rx.get_band_activity')
	.description('Gets the text in the band activity window')
	.action(() => {
		connect()
			.then(js8.rx.getBandActivity)
			.then((activity) => {
				_.each(activity, (message) => {
					// Filter incomplete decodes
					if (message.TEXT != '……' && message.UTC) {
						console.log(
							'%s %shz %s: %s',
							new Date(message.UTC),
							message.OFFSET,
							message.SNR,
							message.TEXT
						);
					}
				});
				process.exit();
			});
	});

program
	.command('rx.get_call_activity')
	.description('Gets the text in the call activity window')
	.action(() => {
		connect()
			.then(js8.rx.getCallActivity)
			.then((activity) => {
				delete activity._ID;
				let calls = Object.keys(activity);
				_.each(calls, (call) => {
					let station = activity[call];
					console.log(
						'%s %s %s %s',
						new Date(station.UTC),
						call,
						station.SNR,
						station.GRID
					);
				});
				process.exit();
			});
	});

program
	.command('rx.get_text')
	.description('Gets the text in the QSO window')
	.action(() => {
		connect()
			.then(js8.rx.getText)
			.then((text) => {
				console.log(text);
				process.exit();
			});
	});

program
	.command('tx.send_message <text>')
	.description('Transmit a message')
	.action((text) => {
		connect().then(() => {
			js8.tx.sendMessage(text).then(() => {
				process.exit();
			});
		});
	})
	.on('--help', function () {
		console.log('');
		console.log('Examples:');
		console.log('');
		console.log('  $ %s tx.send_message "hello, world!"', config.name);
		console.log(
			'  $ %s tx.send_message "@WX Report from JO02mm: $(cat ~/weather/conditions.txt)"',
			config.name
		);
	});

program
	.command('tx.set_text <text>')
	.description('Set the text in the TX window')
	.action((text) => {
		connect().then(() => {
			js8.tx.setText(text).then(() => {
				process.exit();
			});
		});
	})
	.on('--help', function () {
		console.log('');
		console.log('Examples:');
		console.log('');
		console.log('  $ %s tx.set_text "hello, world!"', config.name);
		console.log(
			'  $ %s tx.set_text "@WX Report from JO02mm: $(cat ~/weather/conditions.txt)"',
			config.name
		);
	});

function connect(options) {
	return new Promise((resolve, reject) => {
		// Cmdline options
		let options = program.opts();

		let js8_options = {
			fetch_metadata_at_launch: false,
			tcp: {
				host: options.host,
				port: options.port,
			},
		};

		js8 = require('@trippnology/lib-js8call')(js8_options);

		js8.on('tcp.connected', (connection) => {
			return resolve(connection);
		});

		js8.on('tcp.error', (error) => {
			console.log(error);
			// If we can't connect to JS8Call, we are screwed
			return process.exit(1);
		});
	});
}

program.parse(process.argv);
